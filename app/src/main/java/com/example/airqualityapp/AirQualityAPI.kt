package com.example.airqualityapp

import com.example.airqualityapp.model.MonitoringData
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

interface AirQualityAPI {
    @GET("feed/cluj/?token=c11565b88ba9c85b7b62a3bbaff165c5b7e5389e")
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    fun readData(): Call<MonitoringData>

    companion object {
        private val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }
        private val client = OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
        }.build()

        fun create(): AirQualityAPI {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.waqi.info/")
                .client(client)
                .build()

            return retrofit.create(AirQualityAPI::class.java)
        }
    }
}