package com.example.airqualityapp

import android.util.Log
import android.widget.ListView
import com.example.airqualityapp.adapter.*


object ListHelper {
    /* Source: https://www.tutorialspoint.com/how-to-put-a-listview-into-a-scrollview-without-it-collapsing-on-android-in-kotlin */
    fun getListViewSize(listView: ListView) {
        val listAdapter: AdapterListView = (listView.adapter
            ?: //do nothing return null
            return) as AdapterListView
        //set listAdapter in loop for getting final size
        var totalHeight = 0
        for (size in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(size, null, listView)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }
        //setting listView item in adapter
        val params = listView.layoutParams
        params.height =
            totalHeight + listView.dividerHeight * (listAdapter.count - 1)
        listView.layoutParams = params
        // print height of adapter on log
        Log.i("height of listItem:", totalHeight.toString())
    }

    fun getStatisticsListViewSize(listView: ListView) {
        val listAdapter: AdapterStatisticsListView = (listView.adapter
            ?: //do nothing return null
            return) as AdapterStatisticsListView
        //set listAdapter in loop for getting final size
        var totalHeight = 0
        for (size in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(size, null, listView)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }
        //setting listView item in adapter
        val params = listView.layoutParams
        params.height =
            totalHeight + listView.dividerHeight * (listAdapter.count - 1)
        listView.layoutParams = params
        // print height of adapter on log
        Log.i("height of listItem:", totalHeight.toString())
    }

    fun getRatingsListViewSize(listView: ListView) {
        val listAdapter: AdapterRatingListView = (listView.adapter
            ?: //do nothing return null
            return) as AdapterRatingListView
        //set listAdapter in loop for getting final size
        var totalHeight = 0
        for (size in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(size, null, listView)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }
        //setting listView item in adapter
        val params = listView.layoutParams
        params.height =
            totalHeight + listView.dividerHeight * (listAdapter.count - 1)
        listView.layoutParams = params
        // print height of adapter on log
        Log.i("height of listItem:", totalHeight.toString())
    }
}