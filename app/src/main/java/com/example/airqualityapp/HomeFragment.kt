package com.example.airqualityapp

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context.MODE_PRIVATE
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.airqualityapp.adapter.AdapterListView
import com.example.airqualityapp.model.*
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment() {
    private var airQualityAPI = AirQualityAPI.create()

    private val LOCATION_PERMISSION_REQUEST = 1

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var lastLocation: Location? = null

    private lateinit var cityTV: TextView
    private lateinit var aqiTV: TextView
    private lateinit var airPollutionLevelTV: TextView
    private lateinit var monitoredDataListView: ListView
    private lateinit var airPollutionLevelHelpButton: Button
    private lateinit var adapter: AdapterListView
    private lateinit var parameterData: ArrayList<ParameterData>

    private lateinit var database: DatabaseReference

    private lateinit var helpIcon: Button

    private val sdfNoTime = SimpleDateFormat("dd-MM-yyyy")
    private lateinit var userProfile: UserProfile

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        cityTV = view.findViewById(R.id.home_city_tv)
        aqiTV = view.findViewById(R.id.home_aqi_value_tv)
        airPollutionLevelTV = view.findViewById(R.id.home_air_pollution_level_tv)
        airPollutionLevelHelpButton = view.findViewById(R.id.home_help_icon)
        monitoredDataListView = view.findViewById(R.id.home_monitored_data_lv)
        parameterData = ArrayList()

        database =
            FirebaseDatabase.getInstance("https://airqualityapp-ba64b-default-rtdb.firebaseio.com/").reference

        helpIcon = view.findViewById(R.id.home_help_icon)

        readAqiData()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
        getLocationAccess()
        askQuestion()
    }

    private fun getLocationAccess() {
        if (context?.let { ContextCompat.checkSelfPermission(
                it,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) } == PackageManager.PERMISSION_GRANTED) {
            getLocationUpdates()
            startLocationUpdates()
        }
        else {
            ActivityCompat.requestPermissions(
                context as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST
            )
        }
    }

    private fun getLocationUpdates() {
        locationRequest = LocationRequest()
        locationRequest.interval = 30000
        locationRequest.fastestInterval = 20000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)

        LocationServices
            .getSettingsClient(context)
            .checkLocationSettings(builder.build())
            .addOnFailureListener(context as Activity) { ex ->
                if (ex is ResolvableApiException) {
                    try {
                        ex.startResolutionForResult(
                            context as Activity,
                            100
                        )
                    } catch (sendEx: SendIntentException) { }
                }
            }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult.locations.isNotEmpty()) {
                    if(lastLocation == null) {
                        lastLocation = locationResult.lastLocation
                    } else {
                        val penultimateLocation = lastLocation
                        lastLocation = locationResult.lastLocation
                        if(lastLocation != null && (kotlin.math.abs(penultimateLocation!!.latitude - lastLocation!!.latitude) > 0.0001 || kotlin.math.abs(
                                penultimateLocation!!.longitude - lastLocation!!.longitude
                            ) > 0.0001)) {
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    private fun askQuestion() {
        getUserProfile()
        database.child("questions").addValueEventListener(object :
            ValueEventListener {
            @SuppressLint("ResourceType")
            override fun onDataChange(snapshot: DataSnapshot) {
                val firstrun: Boolean =
                    context?.getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                        ?.getBoolean("firstrun", true) ?: true
                if (firstrun) {
                    val questionsList = arrayListOf<Question>()
                    for (child in snapshot.children) {
                        val childAsQuestion = child.getValue(Question::class.java)
                        if (childAsQuestion?.category == "no profile/birthday/gender") {
                            questionsList.add(childAsQuestion)
                        }
                        if (userProfile.allergies == true && childAsQuestion?.category == "allergies") {
                            questionsList.add(childAsQuestion)
                        }
                        if (userProfile.activePerson == true && childAsQuestion?.category == "active person") {
                            questionsList.add(childAsQuestion)
                        }
                        if (userProfile.respiratoryProblems == true && childAsQuestion?.category == "respiratory problems") {
                            questionsList.add(childAsQuestion)
                        }
                    }
                    if (questionsList != null) {
                        val question = questionsList.random()
                        if (question != null) {
                            if (question.type == "p/n") {
                                context?.let {
                                    MaterialAlertDialogBuilder(it)
                                        .setTitle("Let us know..")
                                        .setMessage(question.questionText)
                                        .setNeutralButton("Ask later") { _, _ ->
                                            // Respond to neutral button press
                                            context!!.getSharedPreferences(
                                                "PREFERENCE",
                                                MODE_PRIVATE
                                            )
                                                .edit().putBoolean("firstrun", false).apply()
                                        }
                                        .setNegativeButton(question.responses?.get(1)) { _, _ ->
                                            // Respond to negative button press
                                            val response = question.responses?.get(1)?.let { it1 ->
                                                UserResponse(
                                                    question.id,
                                                    it1,
                                                    FirebaseAuth.getInstance().currentUser.displayName,
                                                    sdfNoTime.format(
                                                        Date()
                                                    )
                                                )
                                            }
                                            database.child("responses").push().setValue(response)
                                            database.child("statistics")
                                                .child(question.id.toString()).child(
                                                sdfNoTime.format(
                                                    Date()
                                                )
                                            ).child(FirebaseAuth.getInstance().currentUser.uid)
                                                .setValue(
                                                    response!!.response
                                                )
                                            context!!.getSharedPreferences(
                                                "PREFERENCE",
                                                MODE_PRIVATE
                                            )
                                                .edit().putBoolean("firstrun", false).apply()
                                        }
                                        .setPositiveButton(question.responses?.get(0)) { _, _ ->
                                            // Respond to positive button press
                                            val response = question.responses?.get(0)?.let { it1 ->
                                                UserResponse(
                                                    question.id,
                                                    it1,
                                                    FirebaseAuth.getInstance().currentUser.displayName,
                                                    sdfNoTime.format(
                                                        Date()
                                                    )
                                                )
                                            }
                                            database.child("responses").push().setValue(response)
                                            database.child("statistics")
                                                .child(question.id.toString()).child(
                                                sdfNoTime.format(
                                                    Date()
                                                )
                                            ).child(FirebaseAuth.getInstance().currentUser.uid)
                                                .setValue(
                                                    response!!.response
                                                )
                                            context!!.getSharedPreferences(
                                                "PREFERENCE",
                                                MODE_PRIVATE
                                            )
                                                .edit().putBoolean("firstrun", false).apply()
                                        }
                                        .show()
                                }
                            } else if (question.type == "rating") {
                                val dialog = context?.let { Dialog(it) }
                                dialog?.setContentView(R.layout.home_rating_alert_dialog)
                                val questionText =
                                    dialog?.findViewById<TextView>(R.id.home_rating_dialog_title)
                                if (questionText != null) {
                                    questionText.text = question.questionText
                                }
                                val askLaterButton =
                                    dialog?.findViewById<Button>(R.id.home_dialog_ask_later_button)
                                askLaterButton?.setOnClickListener {
                                    context!!.getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                                        .edit().putBoolean("firstrun", false).apply()
                                    dialog.dismiss()
                                }
                                val ratingBar =
                                    dialog?.findViewById<RatingBar>(R.id.home_dialog_rating_bar)
                                ratingBar?.onRatingBarChangeListener =
                                    RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
                                        run {
                                            val response = UserResponse(
                                                question.id,
                                                rating.toString(),
                                                FirebaseAuth.getInstance().currentUser.displayName,
                                                sdfNoTime.format(
                                                    Date()
                                                )
                                            )
                                            database.child("responses").push().setValue(response)
                                            database.child("statistics")
                                                .child(question.id.toString()).child(
                                                sdfNoTime.format(
                                                    Date()
                                                )
                                            ).child(FirebaseAuth.getInstance().currentUser.uid)
                                                .setValue(
                                                    response!!.response
                                                )
                                            Toast.makeText(
                                                context,
                                                "Rating: $rating", Toast.LENGTH_SHORT
                                            ).show()
                                            context!!.getSharedPreferences(
                                                "PREFERENCE",
                                                MODE_PRIVATE
                                            )
                                                .edit().putBoolean("firstrun", false).apply()
                                            dialog?.dismiss()
                                        }
                                    }
                                dialog?.show()
                            }
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                println(error.message)
            }

        })
    }

    private fun getUserProfile(){
        val userProfileRef = database.child("users")
        val queryRef = userProfileRef.orderByChild("name").equalTo(FirebaseAuth.getInstance().currentUser.displayName)
        queryRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val children = snapshot!!.children
                children.forEach {
                    userProfile = it.getValue(UserProfile::class.java)!!
                }
            }

            override fun onCancelled(error: DatabaseError) {
                println(error.message)
            }

        })
    }

    private fun readAqiData() {
        airQualityAPI.readData().enqueue(object : Callback<MonitoringData> {
            override fun onResponse(
                call: Call<MonitoringData>,
                response: Response<MonitoringData>
            ) {
                if (response.isSuccessful) {
                    val data = response.body() as MonitoringData

                    cityTV.text = "Station: " + data.data.city.name
                    setAqiValueAndColor(data.data.aqi)

                    for (par in data.data.iaqi.entries) {
                        parameterData.add(
                            ParameterData(
                                getParameterName(par.key), par.value["v"], chooseUnit(
                                    par.key
                                )
                            )
                        )
                    }
                    adapter = context?.let { AdapterListView(it, parameterData) }!!
                    monitoredDataListView.adapter = adapter
                    ListHelper.getListViewSize(monitoredDataListView)
                }
            }

            override fun onFailure(call: Call<MonitoringData>, t: Throwable) {
                println("Api Problem")
            }
        })
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setAqiValueAndColor(value: Int) {
        aqiTV.text = value.toString()
        when {
            value in 0..50 -> {
                airPollutionLevelTV.text = "Good"
            }
            value in 51..100 -> {
                airPollutionLevelTV.text = "Moderate"
            }
            value in 101..150 -> {
                airPollutionLevelTV.text = "Unhealthy for sensitive groups"
            }
            value in 151..200 -> {
                airPollutionLevelTV.text = "Unhealthy"
            }
            value in 201..300 -> {
                airPollutionLevelTV.text = "Very unhealthy"
            }
            value >= 300 -> {
                airPollutionLevelTV.text = "Hazardous"
            }
        }
        airPollutionLevelHelpButton.setOnClickListener {
            context?.let { it1 ->
                MaterialAlertDialogBuilder(it1)
                    .setTitle("Air Pollution Level: " + airPollutionLevelTV.text)
                    .setMessage(
                        "Health Implications:\n" + getHealthImplications(airPollutionLevelTV.text) + "\nCautionary Statement (for PM2.5):\n" + getCautionaryStatement(
                            airPollutionLevelTV.text
                        )
                    )
                    .setPositiveButton("Ok") { dialog, _ ->
                        dialog.cancel()
                    }
                    .show()
            }
        }
    }

    private fun getHealthImplications(level: CharSequence?): String {
        return when (level) {
            "Good" -> "Air quality is considered satisfactory, and air pollution poses little or no risk."
            "Moderate" -> "Air quality is acceptable; however, for some pollutants there may be a moderate health concern for a very small number of people who are unusually sensitive to air pollution."
            "Unhealthy for sensitive groups" -> "Members of sensitive groups may experience health effects. The general public is not likely to be affected."
            "Unhealthy" -> "Everyone may begin to experience health effects; members of sensitive groups may experience more serious health effects."
            "Very unhealthy" -> "Health warnings of emergency conditions. The entire population is more likely to be affected."
            "Hazardous" -> "Health alert: everyone may experience more serious health effects."
            else -> ""
        }
    }

    private fun getCautionaryStatement(level: CharSequence?): String {
        return when (level) {
            "Good" -> "None."
            "Moderate" -> "Active children and adults, and people with respiratory disease, such as asthma, should limit prolonged outdoor exertion."
            "Unhealthy for sensitive groups" -> "Active children and adults, and people with respiratory disease, such as asthma, should limit prolonged outdoor exertion."
            "Unhealthy" -> "Active children and adults, and people with respiratory disease, such as asthma, should avoid prolonged outdoor exertion; everyone else, especially children, should limit prolonged outdoor exertion."
            "Very unhealthy" -> "Active children and adults, and people with respiratory disease, such as asthma, should avoid all outdoor exertion; everyone else, especially children, should limit outdoor exertion."
            "Hazardous" -> "Everyone should avoid all outdoor exertion."
            else -> ""
        }
    }

    private fun getParameterName(key: String): String {
        return when (key) {
            "dew" -> "Dew Point"
            "h" -> "Humidity"
            "no2" -> "Nitrogen Dioxide"
            "p" -> "Air Pressure"
            "pm10" -> "Particulate Matter < 10um"
            "r" -> "Rain"
            "t" -> "Temperature"
            "w" -> "Wind Speed"
            "pm25" -> "Particulate Matter < 2.5um"
            "o3" -> "Ozone"
            "so2" -> "Sulphur Dioxide"
            "uvi" -> "Ultraviolet (UV) Index"
            else -> ""
        }
    }

    private fun chooseUnit(parameter: String): String {
        return when (parameter) {
            "dew" -> "\u2103"
            "h" -> "%"
            "no2" -> "ug/m\u00B3"
            "p" -> "mbar"
            "pm10" -> "ug/m\u00B3"
            "r" -> "mm"
            "t" -> "\u2103"
            "w" -> "mph"
            "pm25" -> "ug/m\u00B3"
            "o3" -> "ug/m\u00B3"
            "so2" -> "ug/m\u00B3"
            "no2" -> "ug/m\u00B3"
            else -> ""
        }
    }
}