package com.example.airqualityapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.airqualityapp.model.Question
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class MainActivity : AppCompatActivity() {
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var navController: NavController
    private val questionsList: ArrayList<Question> = arrayListOf(
        Question(0,"How do you rate the air quality today?","no profile/birthday/gender","rating", null),
        Question(1,"Is it smoggy now in your city?","no profile/birthday/gender","p/n", arrayListOf("Yes","No")),
        Question(2,"Do you feel that is a lot of dust in the air lately?","no profile/birthday/gender","p/n", arrayListOf("Yes","No")),
        Question(3,"Do you perceive some unusual smell outside?","no profile/birthday/gender","p/n", arrayListOf("Yes","No")),
        Question(4,"How do you evaluate the visibility outside now?","no profile/birthday/gender","rating", null),
        Question(5,"Do your eyes sting? ","no profile/birthday/gender","p/n", arrayListOf("Yes","No")),
        Question(6,"How much do you think air pollution is affecting you?","no profile/birthday/gender","p/n", arrayListOf("Very much","Little to none")),
        Question(7,"Do you feel that the traffic jam and diesel emissions can cause your allergies to manifest?","allergies","p/n", arrayListOf("Yes","No")),
        Question(8,"Going through an area where is a lot of dust is provoking you allergies?","allergies","p/n", arrayListOf("Yes","No")),
        Question(9,"Do you think that air pollution makes your health situation worse?","respiratory problems","p/n", arrayListOf("Yes","No")),
        Question(10,"Can you breath normally while going through a working area?","respiratory problems","p/n", arrayListOf("Yes","No")),
        Question(11,"Do you experience chest pressure while going through an area where you can see a lot of dust?","respiratory problems","p/n", arrayListOf("Yes","No")),
        Question(12,"Do you use the bike as a mean of transport?","active person","p/n", arrayListOf("Yes","No")),
        Question(13,"Do you sometimes prefer to walk instead of using a mean of transport?","active person","p/n", arrayListOf("Yes","No")),
    )
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("firstrun", true).commit()

        database = FirebaseDatabase.getInstance("https://airqualityapp-ba64b-default-rtdb.firebaseio.com/").reference

        /*for (question in questionsList) {
            database.child("questions").push().setValue(question)
        }*/
        bottomNavigationView = findViewById(R.id.bottom_navigation_view)
        navController = findNavController(R.id.fragment)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id)
            {
                R.id.login_fragment -> bottomNavigationView.visibility = View.GONE
                else -> bottomNavigationView.visibility = View.VISIBLE
            }
        }

        bottomNavigationView.setupWithNavController(navController)
    }


}