package com.example.airqualityapp.model

class Question {
    var id: Int = 0
    var questionText: String = ""
    var category: String = ""
    var type: String = ""
    var responses: ArrayList<String>? = arrayListOf()

    constructor() {

    }

    constructor(
        id: Int,
        questionText: String,
        category: String,
        type: String,
        responses: ArrayList<String>?,
    ) {
        this.id = id
        this.questionText = questionText
        this.category = category
        this.type = type
        this.responses = responses
    }
}