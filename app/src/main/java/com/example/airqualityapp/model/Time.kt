package com.example.airqualityapp.model

import java.math.BigInteger

data class Time(val s: String?, val tz: String?, val v: BigInteger?, val iso: String?)