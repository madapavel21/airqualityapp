package com.example.airqualityapp.model

data class RatingData(val questionText: String,val averageValue: Float)
