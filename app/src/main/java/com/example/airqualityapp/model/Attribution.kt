package com.example.airqualityapp.model

data class Attribution(val url: String?, val name: String?, val logo: String?)
