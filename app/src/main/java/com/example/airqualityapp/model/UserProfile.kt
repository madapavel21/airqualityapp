package com.example.airqualityapp.model

class UserProfile {
    var activePerson: Boolean? = false
    var birthday: String? = ""
    var allergies: Boolean? = false
    var gender: String? = ""
    var name: String? = ""
    var respiratoryProblems: Boolean? = false

    constructor() {

    }

    constructor(
        activePerson: Boolean,
        birthday: String,
        allergies: Boolean,
        gender: String?,
        name: String,
        respiratoryProblems: Boolean
    ) {
        this.activePerson = activePerson
        this.birthday = birthday
        this.allergies = allergies
        this.gender = gender
        this.name = name
        this.respiratoryProblems = respiratoryProblems
    }
}