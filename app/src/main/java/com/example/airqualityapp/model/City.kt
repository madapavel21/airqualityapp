package com.example.airqualityapp.model

data class City(val geo: ArrayList<Double>, val name: String, val url: String)