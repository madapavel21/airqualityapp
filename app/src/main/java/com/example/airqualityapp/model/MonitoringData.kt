package com.example.airqualityapp.model

data class MonitoringData(val status: String, val data: Data)