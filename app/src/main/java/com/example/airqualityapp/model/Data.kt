package com.example.airqualityapp.model

data class Data(val aqi: Int, val idx: Int, val attributions: ArrayList<Attribution>?, val city: City, val dominentpol: String?, val iaqi: Map<String,Map<String,Double>>, val time: Time, val forecast: Map<String,Map<String,ArrayList<ForecastParameterValues>>>, val debug: Map<String,String>)