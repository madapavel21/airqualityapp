package com.example.airqualityapp.model

data class StatisticsData(val questionText: String,val statisticText: String,val numberOfPeople: Int,val totalNumber: Int)