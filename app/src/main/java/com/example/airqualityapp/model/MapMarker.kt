package com.example.airqualityapp.model

class MapMarker {
    var user: String = ""
    var markerType: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var date: String = ""

    constructor() {

    }

    constructor(
        user: String,
        markerType: String,
        latitude: Double,
        longitude: Double,
        date: String
    ) {
        this.user= user
        this.markerType = markerType
        this.latitude = latitude
        this.longitude = longitude
        this.date = date
    }
}