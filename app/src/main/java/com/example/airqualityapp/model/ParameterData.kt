package com.example.airqualityapp.model

data class ParameterData(val name: String, val value: Double?, val unit: String)
