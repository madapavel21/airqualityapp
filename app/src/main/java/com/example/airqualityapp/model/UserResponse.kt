package com.example.airqualityapp.model

class UserResponse {
    var questionId: Int = 0
    var response: String = ""
    var user: String = ""
    var date: String = ""

    constructor() {

    }

    constructor(
        questionId: Int,
        response: String,
        user: String,
        date: String,
    ) {
        this.questionId = questionId
        this.response = response
        this.user = user
        this.date = date
    }
}