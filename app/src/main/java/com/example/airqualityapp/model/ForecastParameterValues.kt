package com.example.airqualityapp.model

data class ForecastParameterValues(val avg: Int, val day: String, val max: Int, val min: Int)
