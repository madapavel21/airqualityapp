package com.example.airqualityapp

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.airqualityapp.adapter.CustomInfoWindowAdapter
import com.example.airqualityapp.model.MapMarker
import com.example.airqualityapp.model.UserProfile
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*


class MapsFragment : Fragment(), OnMapReadyCallback {

    private val LOCATION_PERMISSION_REQUEST = 1

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback

    private lateinit var googleMap: GoogleMap

    private lateinit var database: DatabaseReference

    private lateinit var addButton: FloatingActionButton
    private lateinit var weatherButton: FloatingActionButton
    private lateinit var airQualityButton: FloatingActionButton

    private lateinit var weatherLabel: TextView
    private lateinit var airQualityLabel: TextView

    private var clicked: Boolean = false
    private val rotateOpen: Animation by lazy { AnimationUtils.loadAnimation(
        context,
        R.anim.rotate_open_animation
    )}
    private val rotateClose: Animation by lazy { AnimationUtils.loadAnimation(
        context,
        R.anim.rotate_close_animation
    )}
    private val fromBottom: Animation by lazy { AnimationUtils.loadAnimation(
        context,
        R.anim.from_bottom_animation
    )}
    private val toBottom: Animation by lazy { AnimationUtils.loadAnimation(
        context,
        R.anim.to_bottom_animation
    )}

    private lateinit var lastLocation: Location

    private val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm")
    private val sdfNoTime = SimpleDateFormat("dd/MM/yyyy")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?

        database = FirebaseDatabase.getInstance("https://airqualityapp-ba64b-default-rtdb.firebaseio.com/").reference

        val airQualityBottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.bottom_sheet_air_quality))
        airQualityBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN


        val weatherBottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.bottom_sheet_weather))
        weatherBottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

        addButton = view.findViewById(R.id.map_add_fab)
        weatherButton = view.findViewById(R.id.map_weather_fab)
        airQualityButton = view.findViewById(R.id.map_air_quality_fab)

        weatherLabel = view.findViewById(R.id.map_weather_label_fab)
        airQualityLabel = view.findViewById(R.id.map_air_quality_label_fab)

        addButton.setOnClickListener {
            onAddButtonClicked()
        }
        weatherButton.setOnClickListener {
            weatherBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        airQualityButton.setOnClickListener {
            airQualityBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        val aqMarker1: LinearLayout = view.findViewById(R.id.air_quality_marker_1)
        val aqMarker2: LinearLayout = view.findViewById(R.id.air_quality_marker_2)
        val aqMarker3: LinearLayout = view.findViewById(R.id.air_quality_marker_3)
        val aqMarker4: LinearLayout = view.findViewById(R.id.air_quality_marker_4)
        val aqMarker5: LinearLayout = view.findViewById(R.id.air_quality_marker_5)
        aqMarker1.setOnClickListener {
            airQualityBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(aqMarker1.findViewById<TextView>(R.id.air_quality_marker_1_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                aqMarker1.findViewById<TextView>(R.id.air_quality_marker_1_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        aqMarker2.setOnClickListener {
            airQualityBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(aqMarker2.findViewById<TextView>(R.id.air_quality_marker_2_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                aqMarker2.findViewById<TextView>(R.id.air_quality_marker_2_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        aqMarker3.setOnClickListener {
            airQualityBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(aqMarker3.findViewById<TextView>(R.id.air_quality_marker_3_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                aqMarker3.findViewById<TextView>(R.id.air_quality_marker_3_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        aqMarker4.setOnClickListener {
            airQualityBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(aqMarker4.findViewById<TextView>(R.id.air_quality_marker_4_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                aqMarker4.findViewById<TextView>(R.id.air_quality_marker_4_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        aqMarker5.setOnClickListener {
            airQualityBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(aqMarker5.findViewById<TextView>(R.id.air_quality_marker_5_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                aqMarker5.findViewById<TextView>(R.id.air_quality_marker_5_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }

        val weatherMarker1: LinearLayout = view.findViewById(R.id.weather_marker_1)
        val weatherMarker2: LinearLayout = view.findViewById(R.id.weather_marker_2)
        val weatherMarker3: LinearLayout = view.findViewById(R.id.weather_marker_3)
        val weatherMarker4: LinearLayout = view.findViewById(R.id.weather_marker_4)
        val weatherMarker5: LinearLayout = view.findViewById(R.id.weather_marker_5)
        weatherMarker1.setOnClickListener {
            weatherBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(weatherMarker1.findViewById<TextView>(R.id.weather_marker_1_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                weatherMarker1.findViewById<TextView>(R.id.weather_marker_1_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        weatherMarker2.setOnClickListener {
            weatherBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(weatherMarker2.findViewById<TextView>(R.id.weather_marker_2_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                weatherMarker2.findViewById<TextView>(R.id.weather_marker_2_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        weatherMarker3.setOnClickListener {
            weatherBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(weatherMarker3.findViewById<TextView>(R.id.weather_marker_3_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                weatherMarker3.findViewById<TextView>(R.id.weather_marker_3_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        weatherMarker4.setOnClickListener {
            weatherBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(weatherMarker4.findViewById<TextView>(R.id.weather_marker_4_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                weatherMarker4.findViewById<TextView>(R.id.weather_marker_4_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }
        weatherMarker5.setOnClickListener {
            weatherBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastLocation!!.latitude, lastLocation.longitude))
                    .title(weatherMarker5.findViewById<TextView>(R.id.weather_marker_5_label).text as String)
                    .snippet(FirebaseAuth.getInstance().currentUser.displayName + "\n" + sdf.format(Date()))
            )
            val mapMarker = MapMarker(
                FirebaseAuth.getInstance().currentUser.displayName,
                weatherMarker5.findViewById<TextView>(R.id.weather_marker_5_label).text as String,
                lastLocation!!.latitude,
                lastLocation.longitude,
                sdf.format(
                    Date()
                )
            )
            database.child("markers").push().setValue(mapMarker)
        }

        mapFragment?.getMapAsync(this)
    }

    private fun onAddButtonClicked() {
        setVisibility(clicked)
        setAnimation(clicked)
        setClickable(clicked)
        clicked = !clicked
    }

    private fun setVisibility(clicked: Boolean) {
        if(!clicked) {
            weatherLabel.visibility = View.VISIBLE
            airQualityLabel.visibility = View.VISIBLE
            weatherButton.visibility = View.VISIBLE
            airQualityButton.visibility = View.VISIBLE
        } else {
            weatherLabel.visibility = View.INVISIBLE
            airQualityLabel.visibility = View.INVISIBLE
            weatherButton.visibility = View.INVISIBLE
            airQualityButton.visibility = View.INVISIBLE
        }
    }

    private fun setAnimation(clicked: Boolean) {
        if(!clicked) {
            weatherLabel.startAnimation(toBottom)
            airQualityLabel.startAnimation(toBottom)
            weatherButton.startAnimation(toBottom)
            airQualityButton.startAnimation(toBottom)
            addButton.startAnimation(rotateOpen)
        } else {
            weatherLabel.startAnimation(fromBottom)
            airQualityLabel.startAnimation(fromBottom)
            weatherButton.startAnimation(fromBottom)
            airQualityButton.startAnimation(fromBottom)
            addButton.startAnimation(rotateClose)
        }
    }

    private fun setClickable(clicked: Boolean) {
        if(!clicked) {
            weatherButton.isClickable = true
            airQualityButton.isClickable = true
        } else {
            weatherButton.isClickable = false
            airQualityButton.isClickable = false
        }
    }

    private fun getLocationAccess() {
        if (context?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) } == PackageManager.PERMISSION_GRANTED) {
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = true
            getLocationUpdates()
            startLocationUpdates()
        }
        else {
            ActivityCompat.requestPermissions(context as Activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST)
        }
    }

    private fun getLocationUpdates() {
        locationRequest = LocationRequest()
        locationRequest.interval = 30000
        locationRequest.fastestInterval = 20000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)

        LocationServices
            .getSettingsClient(context)
            .checkLocationSettings(builder.build())
            .addOnFailureListener(context as Activity) { ex ->
                if (ex is ResolvableApiException) {
                    try {
                        ex.startResolutionForResult(
                            context as Activity,
                            100
                        )
                    } catch (sendEx: SendIntentException) { }
                }
            }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult.locations.isNotEmpty()) {
                    val location = locationResult.lastLocation
                    if (location != null) {
                        lastLocation = location

                        val cameraPosition = CameraPosition.Builder()
                            .target(LatLng(lastLocation.latitude, lastLocation.longitude))
                            .zoom(17f)
                            .build()
                        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                        addButton.isEnabled = true
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = it
        }

        getLocationAccess()

        googleMap.setInfoWindowAdapter(context?.let { CustomInfoWindowAdapter(it) })

        drawExistingMarkers()
    }

    private fun drawExistingMarkers() {
        val markersRef = database.child("markers")
        val today = sdfNoTime.format(Date())
        val queryRef = markersRef.orderByChild("date").startAt(today).endAt("$today~")
        queryRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val children = snapshot!!.children
                children.forEach {
                    println(it.value)
                    val marker = it.getValue(MapMarker::class.java)
                    googleMap.addMarker(
                        MarkerOptions()
                            .position(LatLng(marker!!.latitude, marker!!.longitude))
                            .title(marker!!.markerType)
                            .snippet(marker!!.user + "\n" + marker!!.date)
                    )
                }
            }

            override fun onCancelled(error: DatabaseError) {
                println(error.message)
            }

        })
    }
}