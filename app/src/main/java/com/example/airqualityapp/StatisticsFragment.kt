package com.example.airqualityapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.example.airqualityapp.adapter.AdapterRatingListView
import com.example.airqualityapp.adapter.AdapterStatisticsListView
import com.example.airqualityapp.model.Question
import com.example.airqualityapp.model.RatingData
import com.example.airqualityapp.model.StatisticsData
import com.example.airqualityapp.model.UserProfile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*

class StatisticsFragment : Fragment() {
    private lateinit var database: DatabaseReference
    private lateinit var statisticsDataListView: ListView
    private var userProfile: UserProfile? = null
    private lateinit var adapter: AdapterStatisticsListView
    private lateinit var statisticsData: ArrayList<StatisticsData>
    private lateinit var ratingsDataListView: ListView
    private lateinit var ratingAdapter: AdapterRatingListView
    private lateinit var ratingsData: ArrayList<RatingData>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_statistics, container, false)

        statisticsDataListView = view.findViewById(R.id.statistics_list_view)
        ratingsDataListView = view.findViewById(R.id.ratings_list_view)

        database =
            FirebaseDatabase.getInstance("https://airqualityapp-ba64b-default-rtdb.firebaseio.com/").reference

        getUserResponses()
        return view
    }

    private fun getUserResponses() {
        getUserProfile()
        database.child("questions").addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                statisticsData = arrayListOf()
                ratingsData = arrayListOf()
                val questionsList = arrayListOf<Question>()
                val idsList = arrayListOf<Int>()
                for (child in snapshot.children) {
                    val childAsQuestion = child.getValue(Question::class.java)
                    if (childAsQuestion?.category == "no profile/birthday/gender") {
                        questionsList.add(childAsQuestion)
                    }
                    if (userProfile != null) {
                        if (userProfile!!.allergies == true && childAsQuestion?.category == "allergies") {
                            questionsList.add(childAsQuestion)
                        }
                        if (userProfile!!.activePerson == true && childAsQuestion?.category == "active person") {
                            questionsList.add(childAsQuestion)
                        }
                        if (userProfile!!.respiratoryProblems == true && childAsQuestion?.category == "respiratory problems") {
                            questionsList.add(childAsQuestion)
                        }
                    }
                }
                for (question in questionsList) {
                    idsList.add(question.id)
                }
                database.child("statistics").addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for (statisticsChild in snapshot.children) {
                            if (statisticsChild.key?.let { idsList.contains(it.toInt()) } == true) {
                                var specificQuestion: Question? = null
                                for (question in questionsList) {
                                    if (question.id == statisticsChild.key?.toInt()) {
                                        specificQuestion = question
                                        break
                                    }
                                }
                                val lastDateKey = getLastDateKey(statisticsChild.children)
                                val lastDate = statisticsChild.child(lastDateKey)
                                if (specificQuestion!!.type == "p/n") {
                                    val totalResponses = lastDate.childrenCount
                                    var countPositive = 0
                                    for (response in lastDate.children) {
                                        if (response.value == specificQuestion!!.responses?.get(0)) {
                                            countPositive++
                                        }
                                    }
                                    val statisticInfo =
                                        countPositive.toString() + " out of " + totalResponses.toString() + " people responded \"" + (specificQuestion.responses?.get(
                                            0
                                        ) ?: "") + "\""
                                    val statistic = StatisticsData(
                                        specificQuestion.questionText,
                                        statisticInfo,
                                        countPositive,
                                        totalResponses.toInt()
                                    )
                                    statisticsData.add(statistic)
                                } else if (specificQuestion!!.type == "rating") {
                                    val totalResponses = lastDate.childrenCount
                                    var sum = 0.0
                                    for (response in lastDate.children) {
                                        sum += response.value.toString().toFloat()
                                    }
                                    var average = sum / totalResponses
                                    val averageAsInt = average.toInt()
                                    average = when {
                                        average - averageAsInt > 0.5 -> {
                                            averageAsInt + 1.0
                                        }
                                        average - averageAsInt == 0.5 -> {
                                            average
                                        }
                                        else -> {
                                            averageAsInt.toDouble()
                                        }
                                    }
                                    val ratingData = RatingData(
                                        specificQuestion.questionText,
                                        average.toFloat()
                                    )
                                    ratingsData.add(ratingData)
                                }
                            }
                        }
                        adapter = context?.let { AdapterStatisticsListView(it, statisticsData) }!!
                        statisticsDataListView.adapter = adapter
                        ListHelper.getStatisticsListViewSize(statisticsDataListView)
                        ratingAdapter = AdapterRatingListView(context!!, ratingsData)
                        ratingsDataListView.adapter = ratingAdapter
                        ListHelper.getRatingsListViewSize(ratingsDataListView)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        println(error.message)
                    }

                })
            }

            override fun onCancelled(error: DatabaseError) {
                println(error.message)
            }

        })
    }

    private fun getLastDateKey(children: Iterable<DataSnapshot>): String {
        var mostRecentTime : Long = 0
        var mostRecentKey = ""
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        for (child in children) {
            val mDate = sdf.parse(child.key)
            if (mDate.time > mostRecentTime) {
                mostRecentTime = mDate.time
                mostRecentKey = child.key.toString()
            }
        }
        return  mostRecentKey
    }

    private fun getUserProfile() {
        val userProfileRef = database.child("users")
        val queryRef = userProfileRef.orderByChild("name").equalTo(FirebaseAuth.getInstance().currentUser.displayName)
        queryRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot != null) {
                    val children = snapshot!!.children
                    children.forEach {
                        userProfile = it.getValue(UserProfile::class.java)!!
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                println(error.message)
            }

        })
    }
}