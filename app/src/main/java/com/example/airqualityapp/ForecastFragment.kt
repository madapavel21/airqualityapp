package com.example.airqualityapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.airqualityapp.model.MonitoringData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class ForecastFragment : Fragment() {
    private var airQualityAPI = AirQualityAPI.create()
    private lateinit var forecastListItemView: View
    private lateinit var parameterListView: View
    private lateinit var mainInflater: LayoutInflater
    private lateinit var mainLinearLayout: LinearLayout
    private lateinit var parameterLinearLayout: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_forecast, container, false)

        mainInflater = inflater
        mainLinearLayout = view.findViewById(R.id.forecast_ll)

        readForecastData()
        return view
    }

    private fun readForecastData() {
        airQualityAPI.readData().enqueue(object : Callback<MonitoringData> {
            override fun onResponse(
                call: Call<MonitoringData>,
                response: Response<MonitoringData>
            ) {
                if (response.isSuccessful) {
                    val data = response.body() as MonitoringData

                    val forecast = data.data.forecast.entries
                    for (entry in forecast) {
                        for(par in entry.value) {
                            forecastListItemView = mainInflater.inflate(R.layout.forecast_list_item, view as ViewGroup, false)

                            parameterLinearLayout = forecastListItemView.findViewById(R.id.forecast_parameter_ll)

                            val parameterName = forecastListItemView.findViewById<TextView>(R.id.forecast_li_parameter_tv)
                            parameterName.text = getParameterName(par.key)

                            val unit = chooseUnit(par.key)

                            for (forecastData in par.value) {
                                parameterListView = mainInflater.inflate(R.layout.forecast_parameter_list_item, forecastListItemView as ViewGroup, false)

                                val date = parameterListView.findViewById<TextView>(R.id.forecast_parameter_li_date)
                                val avg = parameterListView.findViewById<TextView>(R.id.forecast_parameter_li_avg)
                                val max = parameterListView.findViewById<TextView>(R.id.forecast_parameter_li_max)
                                val min = parameterListView.findViewById<TextView>(R.id.forecast_parameter_li_min)

                                val day = SimpleDateFormat("yyyy-MM-dd").parse(forecastData.day)
                                date.text = SimpleDateFormat("EE\ndd/MM/yyyy").format(day).toString()
                                avg.text = forecastData.avg.toString() + " " + unit
                                max.text = forecastData.max.toString() + " " + unit
                                min.text = forecastData.min.toString() + " " + unit

                                parameterLinearLayout.addView(parameterListView)
                            }
                            mainLinearLayout.addView(forecastListItemView)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<MonitoringData>, t: Throwable) {
                println("Api Problem")
            }
        })
    }

    private fun getParameterName(key: String): String {
        return when (key) {
            "dew" -> "Dew Point"
            "h" -> "Humidity"
            "no2" -> "Nitrogen Dioxide"
            "p" -> "Air Pressure"
            "pm10" -> "Particulate Matter < 10um"
            "r" -> "Rain"
            "t" -> "Temperature"
            "w" -> "Wind Speed"
            "pm25" -> "Particulate Matter < 2.5um"
            "o3" -> "Ozone"
            "so2" -> "Sulphur Dioxide"
            "no2" -> "Nitrogen Dioxide"
            "uvi" -> "Ultraviolet (UV) Index"
            else -> ""
        }
    }

    private fun chooseUnit(parameter: String): String {
        return when (parameter) {
            "dew" -> "\u2103"
            "h" -> "%"
            "no2" -> "ug/m\u00B3"
            "p" -> "mbar"
            "pm10" -> "ug/m\u00B3"
            "r" -> "mm"
            "t" -> "\u2103"
            "w" -> "mph"
            "pm25" -> "ug/m\u00B3"
            "o3" -> "ug/m\u00B3"
            "so2" -> "ug/m\u00B3"
            "no2" -> "ug/m\u00B3"
            else -> ""
        }
    }
}