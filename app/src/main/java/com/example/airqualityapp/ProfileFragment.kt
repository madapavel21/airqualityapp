package com.example.airqualityapp

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.airqualityapp.model.UserProfile
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import java.util.*


class ProfileFragment : Fragment() {
    private lateinit var navController: NavController

    private lateinit var database: DatabaseReference

    private lateinit var signOutButton: MaterialButton
    private lateinit var saveButton: MaterialButton
    private lateinit var helloUserTextView: TextView
    private lateinit var birthdayTextInputEditText: TextInputEditText
    private lateinit var genderDropDownMenuAutoCompleteTextView: AutoCompleteTextView
    private lateinit var myCharacteristicsChipGroup: ChipGroup

    private lateinit var username: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val profileView = inflater.inflate(R.layout.fragment_profile, container, false)

        database = FirebaseDatabase.getInstance("https://airqualityapp-ba64b-default-rtdb.firebaseio.com/").reference

        signOutButton = profileView.findViewById(R.id.profile_sign_out)

        helloUserTextView = profileView.findViewById(R.id.profile_title)
        username = FirebaseAuth.getInstance().currentUser?.displayName.toString()
        helloUserTextView.text = "Hello, " + username + "!"

        val currentTime = Calendar.getInstance()
        val year = currentTime.get(Calendar.YEAR)
        val month = currentTime.get(Calendar.MONTH)
        val day = currentTime.get(Calendar.DAY_OF_MONTH)

        val datePicker = context?.let { it1 ->
            DatePickerDialog(it1,
                { _, year, month, dayOfMonth -> birthdayTextInputEditText.setText(String.format("%d/%d/%d", dayOfMonth, month + 1, year)) }, year, month, day)
        };

        birthdayTextInputEditText = profileView.findViewById(R.id.profile_birthday_input)
        birthdayTextInputEditText.setOnClickListener{
            datePicker?.show()
        }

        val genderValues = resources.getStringArray(R.array.gender)
        val adapter = context?.let { ArrayAdapter(it,R.layout.profile_gender_list_item,genderValues) }
        genderDropDownMenuAutoCompleteTextView = profileView.findViewById(R.id.profile_gender_input)
        genderDropDownMenuAutoCompleteTextView.setAdapter(adapter)

        myCharacteristicsChipGroup = profileView.findViewById(R.id.profile_chipgroup)

        saveButton = profileView.findViewById(R.id.profile_save)

        autofillProfile()

        return profileView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        signOutButton.setOnClickListener { launchSignOut() }
        saveButton.setOnClickListener { updateUserProfile() }
    }

    private fun updateUserProfile() {
        var allergies = false
        var respiratoryProblems = false
        var activePerson = false
        val myCharacteristicsIds = myCharacteristicsChipGroup.checkedChipIds
        for(id in myCharacteristicsIds) {
            when (myCharacteristicsChipGroup.findViewById<Chip>(id).text.toString()) {
                "I am allergic" -> allergies = true
                "I have respiratory problems" -> respiratoryProblems = true
                "Outdoor active person" -> activePerson = true
            }
        }
        val userProfile: UserProfile = if(genderDropDownMenuAutoCompleteTextView.text.toString() == "M" || genderDropDownMenuAutoCompleteTextView.text.toString() == "F") {
            UserProfile(activePerson,birthdayTextInputEditText.text.toString(),allergies,
                genderDropDownMenuAutoCompleteTextView.text.toString(),username,respiratoryProblems)
        } else {
            UserProfile(activePerson,birthdayTextInputEditText.text.toString(),allergies,
                null,username,respiratoryProblems)
        }
        database.child("users").child(FirebaseAuth.getInstance().currentUser.uid).setValue(userProfile)
        Toast.makeText(context,"Profile saved!",Toast.LENGTH_SHORT).show()
    }

    private fun autofillProfile() {
        var user: UserProfile? = null
        val userProfileRef = database.child("users")
        val queryRef = userProfileRef.orderByChild("name").equalTo(username)
        queryRef.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val children = snapshot!!.children
                children.forEach {
                    println(it.value)
                    user = it.getValue(UserProfile::class.java)
                    birthdayTextInputEditText.setText(user!!.birthday)
                    genderDropDownMenuAutoCompleteTextView.setText(user!!.gender)
                    if(user!!.allergies == true) {
                        myCharacteristicsChipGroup.check(R.id.profile_allergies_chip)
                    }
                    if(user!!.respiratoryProblems == true) {
                        myCharacteristicsChipGroup.check(R.id.profile_respiratory_problems_chip)
                    }
                    if(user!!.activePerson == true) {
                        myCharacteristicsChipGroup.check(R.id.profile_active_person_chip)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                println(error.message)
            }

        })
    }

    private fun launchSignOut() {
        FirebaseAuth.getInstance().signOut()
        navController.navigate(R.id.login_fragment)
    }

}