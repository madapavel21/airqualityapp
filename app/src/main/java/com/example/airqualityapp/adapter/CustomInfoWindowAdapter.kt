package com.example.airqualityapp.adapter

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.airqualityapp.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class CustomInfoWindowAdapter(val context: Context): GoogleMap.InfoWindowAdapter {
    private val view = LayoutInflater.from(context).inflate(R.layout.map_custom_info_window,null)

    private var title: TextView = view.findViewById(R.id.map_ciw_title)
    private var snippet: TextView = view.findViewById(R.id.map_ciw_snippet)
    private var icon: ImageView = view.findViewById(R.id.map_ciw_icon)

    override fun getInfoWindow(marker: Marker?): View {
        if (marker != null) {
            title.text = marker.title
            snippet.text = marker.snippet
            setImage(marker.title)
        }
        return view
    }

    override fun getInfoContents(marker: Marker?): View {
        if (marker != null) {
            title.text = marker.title
            snippet.text = marker.snippet
            setImage(marker.title)
        }

        return view
    }

    private fun setImage(title: String) {
        when (title) {
            context.resources.getString(R.string.working_area) -> icon.setImageResource(R.drawable.ic_working_area)
            context.resources.getString(R.string.unusual_smell)-> icon.setImageResource(R.drawable.ic_unusual_smell)
            context.resources.getString(R.string.dust) -> icon.setImageResource(R.drawable.ic_low_visibility)
            context.resources.getString(R.string.traffic_jam) -> icon.setImageResource(R.drawable.ic_traffic_jam)
            context.resources.getString(R.string.stifling_air) -> icon.setImageResource(R.drawable.ic_stifling_air)
            context.resources.getString(R.string.suffocating_air) ->  icon.setImageResource(R.drawable.ic_suffocating_air)
            context.resources.getString(R.string.gale) ->  icon.setImageResource(R.drawable.ic_gale)
            context.resources.getString(R.string.frost) ->  icon.setImageResource(R.drawable.ic_frost)
            context.resources.getString(R.string.storm) -> icon.setImageResource(R.drawable.ic_storm)
            context.resources.getString(R.string.fog) -> icon.setImageResource(R.drawable.ic_fog)
        }
    }
}