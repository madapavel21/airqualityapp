package com.example.airqualityapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RatingBar
import android.widget.TextView
import com.example.airqualityapp.R
import com.example.airqualityapp.model.RatingData

class AdapterRatingListView(var context: Context, var ratingsData: ArrayList<RatingData>): BaseAdapter() {

    private lateinit var questionText: TextView
    private lateinit var averageText: TextView
    private lateinit var ratingBar: RatingBar

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(
            R.layout.ratings_list_item,
            parent,
            false
        )

        questionText = view.findViewById(R.id.ratings_lv_question_text)
        averageText = view.findViewById(R.id.ratings_lv_average_text)
        ratingBar = view.findViewById(R.id.ratings_lv_rating_bar)

        questionText.text = ratingsData[position].questionText
        averageText.text = "The average value of responses is " + ratingsData[position].averageValue
        ratingBar.rating = ratingsData[position].averageValue

        return view
    }

    override fun getCount(): Int {
        return ratingsData.size
    }

    override fun getItem(position: Int): Any {
        return ratingsData[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

}