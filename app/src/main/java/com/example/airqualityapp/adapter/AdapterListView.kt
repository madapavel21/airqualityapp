package com.example.airqualityapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.airqualityapp.R
import com.example.airqualityapp.model.ParameterData
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class AdapterListView(var context: Context, var monitoredData: ArrayList<ParameterData>): BaseAdapter() {

    private lateinit var parameterName: TextView
    private lateinit var parameterValue: TextView
    private lateinit var parameterUnit: TextView
    private lateinit var infoIcon: Button

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = LayoutInflater.from(context).inflate(
            R.layout.monitored_data_list_item,
            parent,
            false
        )

        parameterName = view.findViewById(R.id.list_item_parameter)
        parameterValue = view.findViewById(R.id.list_item_parameter_value)
        parameterUnit = view.findViewById(R.id.list_item_parameter_unit)
        infoIcon = view.findViewById(R.id.list_item_info_icon)

        parameterName.text = monitoredData[position].name
        parameterValue.text = monitoredData[position].value.toString()
        parameterUnit.text = monitoredData[position].unit
        infoIcon.setOnClickListener {
            MaterialAlertDialogBuilder(context)
                .setTitle(monitoredData[position].name)
                .setMessage("Value: " + monitoredData[position].value.toString() + "\nUnit: " + monitoredData[position].unit + "\n" + getInfo(monitoredData[position].name))
                .setPositiveButton("Ok") { dialog, _ ->
                    dialog.cancel()
                }
                .show()
        }
        return view
    }

    private fun getInfo(parName: String): String {
        return when (parName) {
            "Dew Point" -> "The dew point is the temperature to which air must be cooled to become saturated with water vapor."
            "Humidity" -> "Humidity is the concentration of water vapour present in the air."
            "Nitrogen Dioxide" -> "Very toxic gas with a strong, stifling odor, that can cause respiratory tract irritation.\nChildren are most affected."
            "Air Pressure" -> "Air pressure is the pressure within the atmosphere of Earth."
            "Particulate Matter < 10um" -> "Daily limit for human health protection: 50 ug/m³"
            "Rain" -> "The major cause of rain production is moisture moving along three-dimensional zones of temperature and moisture contrasts known as weather fronts."
            "Temperature" -> "Temperature is a physical quantity that expresses hot and cold."
            "Wind Speed" -> "In meteorology, wind speed is a fundamental atmospheric quantity caused by air moving from high to low pressure, usually due to changes in temperature."
            "Particulate Matter < 2.5um" -> "Annually target value: 25 ug/m³"
            "Ozone" -> "Gas with a very stifling odor.\nThe concentration of ozone in the soil causes respiratory tract and eye irritation."
            "Sulphur Dioxide" -> "Hourly limit for human health protection: 350 ug/m³\n When the concentration is high, children, elderly persons, people with asthma or other respiratory problems may be affected."
            "Ultraviolet (UV) Index" -> "0->2: You can safely enjoy being outside!\n3->7:Seek shade during midday hours! Slip on a shirt, slop on sunscreen and slap on hat!\n8:Avoid being outside during midday hours! Make sure you seek shade! Shirt, sunscreen and hat are a must!"
            else -> ""
        }
    }

    override fun getCount(): Int {
        return monitoredData.size
    }

    override fun getItem(position: Int): Any {
        return monitoredData[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


}