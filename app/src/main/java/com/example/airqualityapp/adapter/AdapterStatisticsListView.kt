package com.example.airqualityapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ProgressBar
import android.widget.TextView
import com.example.airqualityapp.R
import com.example.airqualityapp.model.StatisticsData

class AdapterStatisticsListView(var context: Context, var statisticsData: ArrayList<StatisticsData>): BaseAdapter() {

    private lateinit var questionText: TextView
    private lateinit var statisticText: TextView
    private lateinit var statisticValue: TextView
    private lateinit var progressBar: ProgressBar

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = LayoutInflater.from(context).inflate(
            R.layout.statistics_list_item,
            parent,
            false
        )

        questionText = view.findViewById(R.id.statistics_question_text)
        statisticText = view.findViewById(R.id.statistics_text)
        statisticValue = view.findViewById(R.id.statistics_value)
        progressBar = view.findViewById(R.id.statistics_progress_bar)

        questionText.text = statisticsData[position].questionText
        statisticText.text = statisticsData[position].statisticText
        statisticValue.text = "${statisticsData[position].numberOfPeople}/${statisticsData[position].totalNumber}"

        progressBar.progress = (statisticsData[position].numberOfPeople * 100) / statisticsData[position].totalNumber
        return view
    }

    override fun getCount(): Int {
        return statisticsData.size
    }

    override fun getItem(position: Int): Any {
        return statisticsData[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

}